;;; montage.el --- photomontage with Stable Diffusion  -*- lexical-binding: t; -*-

;; Copyright (C) 2022 David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>>
;; Keywords: multimedia, tools

;; License:

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;; Commentary:

;; NOTE: This program is under construction and not all features are
;; implemented. Documentation is forthcoming.

;; This program, "Emacs Montage", assists in generating families of
;; related images with Stable Diffusion and turning them into
;; high-definition thematic animated photomontages synced to
;; user-supplied music/audio. It functions as an Emacs library that
;; generates various scripts for Python, FFMPEG, ImageMagick, and
;; other external commands.

;; Requirements:

;;   - A fast computer running Linux
;;   - GNU Emacs 28+
;;   - FFMPEG 4.4.2+
;;   - ImageMagick 7.0+
;;   - GNU Parallel
;;   - Python and Pip (or Anaconda)
;;   - Pytorch
;;   - Huggingface.co Diffusers with Stable Diffusion v1.4
;;   - An AI image upscaler (such as RealESRGAN) available from the
;;     command line
;;   - 16 GB of RAM, a discrete GPU, and plenty of free disk space are
;;     recommended.

;; Features: 

;;   Defining variables for each chapter including video resolution,
;;   frame rate, audio file, musical tempo, scene vocabularies, and
;;   prompt templates.

;;   Exploratory batch jobs where a prompt template is expanded
;;   differently for each generated image in a scene. Noisy prompts
;;   are supported via randomized selection from supplied word lists
;;   and/or user-supplied functions. Style cues can be injected
;;   uniformly on a scene-specific basis as other aspects of the
;;   prompt are made to vary.

;;   Culling unwanted images with image-dired and assembling wanted
;;   images into a visual org-mode timeline with specifiable image
;;   durations and transforms.

;;   Specifying sequences of images to be shown in a scene, musical
;;   beat/measure onset, duration, pan/crop/zoom parameters of each
;;   image, and image transitions in tabular org-mode format and
;;   rendering these to video using imagemagick and ffmpeg. Various
;;   photomontage techniques in the styles of Ken Burns and Charles
;;   Braverman are included as pluggable functions.

;;; Code:

(eval-when-compile (require 'cl))
(require 'cl-lib)

;;; Configuration variables

(defgroup montage nil
  "Emacs Montage makes slide shows from Stable Diffusion or other images."
  :group 'applications)

(defcustom montage-source-directory "~/montage/"
  "Directory where montage.el and its associated files are stored."
  :tag "Montage source directory"
  :group 'montage
  :type 'directory)

(defcustom montage-project-directory "~/crisis/"
  "Directory for the current project."
  :tag "Montage project directory"
  :group 'montage
  :type 'directory)

(defcustom montage-diffusion-directory "~/dockerx/"
  "Directory where diffusion scripts and output images are stored."
  :tag "Montage diffusion directory"
  :group 'montage
  :type 'directory)

(defcustom montage-magick-program "/usr/local/bin/magick"
  "Filename of ImageMagick executable."
  :tag "Montage Magick Program"
  :group 'montage
  :type 'string)

(defcustom montage-magick-directory "~/crisis/magick/"
  "Directory where ImageMagick scripts and output images are stored."
  :tag "Montage Magick directory"
  :group 'montage
  :type 'directory)

(defcustom montage-ffmpeg-program "ffmpeg"
  "Filename of FFmpeg executable."
  :tag "Montage FFmpeg Program"
  :group 'montage
  :type 'string)

(defcustom montage-ffmpeg-directory "~/crisis/ffmpeg/"
  "Directory where FFmpeg scripts and output images are stored."
  :tag "Montage FFmpeg directory"
  :group 'montage
  :type 'directory)

(defcustom montage-upscale-program (expand-file-name "~/dockerx/RealESRGAN/realesrgan-ncnn-vulkan")
  "Filename of upscaler."
  :tag "Montage upscale program"
  :group 'montage
  :type 'string)

(defcustom montage-upscale-model-directory (expand-file-name "~/dockerx/RealESRGAN/models")
  "Directory where upscaler models are stored."
  :tag "Montage upscale model directory"
  :group 'montage
  :type 'directory)

(defcustom montage-upscaled-directory "~/crisis/upscaled/"
  "Directory where upscaled images are stored."
  :tag "Montage upscaled images directory"
  :group 'montage
  :type 'directory)

(defcustom montage-container-script "~/dockerx/montage-diffuse.sh"
  "Filename of diffusion script.")
;; the following path is only mapped this way inside the SD container.
;; the purpose of this variable is to embed filenames into python
;; scripts that will run in the container.
(defvar montage-container-path-prefix "/dockerx/")

(defvar montage-new-directory "~/crisis/new/")
(defvar montage-raw-directory  "~/crisis/raw/")

(defvar montage-inhibit-render nil)

(defcustom montage-maximum-jobs 12
  "Number of parallel jobs to run during rendering."
  :tag "Montage maximum jobs"
  :group 'montage
  :type 'integer)

(defvar montage-inhibit-cache nil
  "When non-nil, emit a complete and idempotent render script that
doesn't use caching.")

;;; Per-project variables

(defcustom montage-video-width 2560 "Width in pixels of output video."
  :tag "Montage video width"
  :group 'montage
  :type 'integer)

(defcustom montage-video-height 1440 "Height in pixels of output video."
  :tag "Montage video height"
  :group 'montage
  :type 'integer)

(defcustom montage-audio-file nil "Filename of accompanying audio, or nil for silence."
  :tag "Montage audio file"
  :group 'montage
  :type 'file)

(defcustom montage-fps 24 "Number of frames per second."
  :tag "Montage frames per second (FPS)"
  :group 'montage
  :type 'integer)

(defcustom montage-bpm 140 "Number of beats per minute."
  :tag "Montage beats per minute (BPM)"
  :group 'montage
  :type 'number)

(defcustom montage-upscale-factor 4 "Integer upscale factor."
  :tag "Montage upscale factor"
  :group 'montage
  :type 'integer)
  
(defvar montage-zoom-maximum 10.0)

(defun montage-log (format-string &rest args)
  (apply 'message format-string args)
  ;; print to stdout in batch mode
  (apply 'prin1 (list (apply 'format format-string args))))

(defun montage-log-raw (string)
  (message "%s" string)
  (prin1 (list string)))

;;; Scripting

(defvar montage-shell-script nil)
(defun montage-clear-script () (setf montage-shell-script nil))
(defun montage-emit-script (command) (push command montage-shell-script))
(defun montage-current-script () (reverse montage-shell-script))
(defun montage-script-file () (expand-file-name "montage.sh" montage-project-directory))

(defun montage-save-script ()
  (interactive)
  (with-temp-buffer
    (cl-dolist (command (montage-current-script))
      (insert (concat command "\n")))
    (write-file (montage-script-file))))

(defvar montage-timer nil)
(defvar montage-shell-buffer-name "*squeak*")
(defvar montage-last-line "")
(defvar montage-shell-process nil)
(defvar montage-update-number 0)

;; this is used by the Squeak frontend. 
(defun montage-run-script ()
  (interactive)
  (get-buffer-create montage-shell-buffer-name)
  (setf montage-timer
        (run-at-time 1.0 1.0
                     (lambda ()
                       (save-window-excursion
                         (save-excursion
                           (switch-to-buffer montage-shell-buffer-name)
                           (goto-char (point-max))
                           (goto-char (point-at-bol))
                           (previous-line)
                           (let ((str (buffer-substring-no-properties
                                       (point) (point-at-eol))))
                             (when (not (string= str montage-last-line))
                               (setf montage-last-line str)
                               (montage-log-raw str)))
                           (cl-incf montage-update-number)
                           (montage-log-raw
                            ;; print extra dots to fill emacs output buffer
                            (concat (make-string 256 ?,)
                                    "Processing..."
                                    (make-string 256 ?.)
                                    "Processing..."
                                    (make-string 256 ?')
                                    "Processing..."
                                    (make-string 256 ?`)))
                           )))))
  (setf montage-shell-process
        (start-process-shell-command "*montage*"
                                     montage-shell-buffer-name
                                     (format "2>&1 sh -x %s" (montage-script-file))))
  (sleep-for 1.0)
  (while (process-live-p montage-shell-process)
    (sleep-for 1.0))
  (kill-emacs))
                        
;;; Default random seed generator

(defun montage-random-seed () (random 298473578))

;;; Envelope generator for sweeping coordinates

(defun montage-env-pairs (env)
  (let (pairs)
    (while env
      (push (cl-subseq env 0 2) pairs)
      (setf env (cl-subseq env 2)))
    (reverse pairs)))

(defun montage-clamp (x low high)
  (cond ((< x low) low)
        ((> x high) high)
        (t x)))

(defun montage-interp-linear (x1 y1 desired-x x2 y2)
  (cl-assert (<= x1 desired-x x2))
  (let* ((dist-x (- x2 x1))
         (pos-x (- desired-x x1))
         (a (/ pos-x dist-x))
         (dist-y (- y2 y1))
         (pos-y (* a dist-y)))
    (+ y1 pos-y)))

(defun montage-interp-smootherstep (x1 y1 desired-x x2 y2)
  (let ((x (montage-clamp (/ (- desired-x x1)
                             (- x2 x1))
                          0.0
                          1.0)))
    ;; start at left edge's y-coord
    (+ y1
       ;; Perlin's polynomial from Wikipedia
       (* (* x x x (+ (* x (- (* x 6) 15)) 10))
          ;; scale by range so that output of x=1.0 from polynomial
          ;; goes to right edge's y-coord
          (- y2 y1)))))

(cl-defun montage-env-interp (env desired-x &optional (interp-func 'montage-interp-linear))
  (let ((pair nil)
        (pairs (montage-env-pairs env)))
    (cl-block interp
      (while pairs
        (setf pair (pop pairs))
        (cl-destructuring-bind (x y) pair
          (if (and (>= desired-x x)
                   (or (null pairs)
                       ;; peek ahead at next x value
                       (< desired-x (cl-first (cl-first pairs)))))
              (if (null pairs)
                  (cl-return-from interp y)
                (cl-return-from interp
                  (funcall interp-func
                           x y
                           desired-x
                           (cl-first (cl-first pairs))
                           (cl-second (cl-first pairs)))))))))))

;;; Imagemagick operations

(cl-defun montage-image-size (file)
  (with-temp-buffer
    (shell-command (concat montage-magick-program
                           " identify -format \"(%w %h)\" "
                           file)
                   (current-buffer))
    (read (buffer-substring-no-properties (point-min) (point-max)))))

(cl-defun montage-image-width (file)
  (first (montage-image-size file)))

(cl-defun montage-image-height (file)
  (second (montage-image-size file)))

(cl-defun montage-image-square-p (file)
  (let ((spec (montage-image-size file)))
    (= (first spec) (second spec))))

(cl-defun montage-image-wide-p (file)
  (let ((spec (montage-image-size file)))
    (> (first spec) (second spec))))

(cl-defun montage-image-tall-p (file)
  (let ((spec (montage-image-size file)))
    (< (first spec) (second spec))))

(defcustom montage-magick-use-opencl nil
  "When non-nil, emit OpenCL env vars for ImageMagick."
  :tag "Montage Magick use OpenCL"
  :group 'montage
  :type 'boolean)

(defun montage-magick-opencl-flag ()
  (if montage-magick-use-opencl
      "MAGICK_OCL_DEVICE=true"
    ""))

(cl-defun montage-magick-command (input-file output-file op-string)
  (format "%s %s -limit thread 1 %s %s %s"
          (montage-magick-opencl-flag)
          montage-magick-program
          input-file
          op-string
          output-file))

(cl-defun montage-resize (&key width height)
  (format "-resize %dx%d" width height))

(cl-defun montage-crop (&key (width montage-video-width) (height montage-video-height)
                             (zoom 1.0)
                             x y
                             )
  ;; we print 32 digits after the decimal point to ensure smooth
  ;; scrolling
  (format "-define distort:viewport=%dx%d+0+0 +distort SRT %9.32f,%9.32f,%f,0.0,0.0,0.0"
          (truncate width) (truncate height)
          (float x) (float y)
          zoom))

(defcustom montage-pixels-per-speed-unit 100
  "Number of pixels per 1.0 transition speed units."
  :tag "Montage pixels per speed unit"
  :group 'montage
  :type 'integer)

(cl-defun montage-pan-down (&key (speed 1.0))
  ;; leave as a list for now, since the envelope is expanded later
  `(montage-crop :zoom 1.3 :x 0 :y (0 0 1 ,(* montage-pixels-per-speed-unit speed))))

(cl-defun montage-pan-left (&key (speed 1.0))
  `(montage-crop :zoom 1.2 :x (0 ,(- 1024 (* montage-pixels-per-speed-unit speed)) 1 0) :y 0))

(cl-defun montage-pre-expand-op-form (form)
  ;; vector specs in the transform column must be evaluated
  (if (vectorp form)
      (eval (append (list (aref form 0))
                    (append (cl-subseq form 1) nil)))
    form))

(cl-defun montage-expand-op (op x &rest args)
  "Expand any envelopes in ARGS with argument X. Calls the specified OP,
which returns a string with Imagemagick commands."
  (cl-assert (symbolp op))
  (cl-assert (numberp x))
  (cl-assert (keywordp (first args)))
  (apply op (apply 'append (mapcar (lambda (pair)
                                     (list (first pair)
                                           (if (listp (second pair))
                                               ;; expand envelope here
                                               (montage-env-interp (second pair) x)
                                             (second pair))))
                                   (montage-env-pairs args)))))

(cl-defun montage-sweep-op (op n &rest args)
  "Expand the envelopes in ARGS to create N calls to OP with
interpolated values."
  (cl-assert (symbolp op))
  (cl-assert (numberp n))
  (cl-assert (keywordp (first args)))
  (let (results)
    (dotimes (i n)
      (let ((x (* i (/ 1.0 (float n))))) ;; sweep x across 0.0-1.0 for n frames
        (push (apply 'montage-expand-op op x args) results)))
    (reverse results)))

(cl-defun montage-output-frame-file (n)
  (expand-file-name (format "frame-%d.png" n) montage-magick-directory))

(cl-defun montage-output-frame-script-file (n)
  (expand-file-name (format "frame-%d.sh" n) montage-magick-directory))

(cl-defun montage-op-strings (num-frames ops)
  (cl-assert (integerp num-frames))
  (cl-assert (listp ops))
  ;; (cl-assert (vectorp (first ops)))
  (cl-assert (symbolp (first (first ops))))
  (mapcar (lambda (op)
            (apply 'montage-sweep-op
                   (first op)
                   num-frames
                   (rest op)))
          ops))

(defun montage-frame-files (num-frames)
  (let (files)
    (dotimes (i num-frames)
      (push (montage-output-frame-file i) files))
    (nreverse files)))

(defun montage-frame-scripts (num-frames)
  (let (files)
    (dotimes (i num-frames)
      (push (montage-output-frame-script-file i) files))
    (nreverse files)))

(cl-defun montage-magick-expand-image-commands (input-file num-frames ops)
  (let* ((op-strings* (montage-op-strings num-frames ops))
         (output-files (montage-frame-files num-frames))
         (op-strings (first op-strings*)))
    (mapcar (lambda (op-string)
              (montage-magick-command input-file (pop output-files) op-string))
            op-strings)))

;;; Calculating time on the beat

(defun montage-seconds->frames (seconds)
  (round (* (float montage-fps) (float seconds))))
  
(defun montage-beat->frames (beat)
  (truncate (montage-beat->frames* beat)))

(defun montage-beat->frames* (beat)
  (let* ((beats-per-minute montage-bpm)
         (seconds-per-minute 60.0)
         (seconds-per-beat (/ 1.0 (/ (float beats-per-minute) seconds-per-minute)))
         (frames-per-beat (* (float montage-fps) seconds-per-beat)))
    (* (float beat) (float frames-per-beat))))

(defun montage-beat-length (n)
  ;; typically each beat is not an integral number of frames. here we
  ;; calculate the correct length for each block in order avoid drift.
  (- (montage-beat->frames* (1+ n))
               (montage-beat->frames* n)))

(defvar montage-current-beat-index 0)

(defun montage-span-length (duration beat-index)
  (round (* duration (montage-beat-length beat-index))))

;;; Combining PNG frames into a cached video fragment

(defun montage-ffmpeg-image-file-args (file)
  (format "-i %s" file))

(defun montage-ffmpeg-output-file (file)
  (expand-file-name
   (concat (file-name-sans-extension (file-name-nondirectory file)) ".mp4")
   montage-ffmpeg-directory))

(defun montage-ffmpeg-script-file (file)
  (expand-file-name
   (concat (file-name-sans-extension (file-name-nondirectory file)) ".ffm")
   montage-ffmpeg-directory))

(cl-defun montage-clear-frames ()
  (montage-emit-script (format "rm %sframe*.png \n" (file-name-as-directory montage-magick-directory))))

(cl-defun montage-combine-frames (output-file)
  (montage-emit-script
   (format "ffmpeg -y -framerate %d -i %s -c:v libx264 %s\n"
           montage-fps
           ;; the percent sign on the following line tells FFMPEG to
           ;; read all the files with names fitting the pattern.
           ;; It looks like an Emacs format directive, but isn't.
           (concat (file-name-as-directory montage-magick-directory) "frame-%d.png")
           output-file)))

;;; Concatenating chosen video fragments

(cl-defun montage-concatenate-videos (images &optional (audio montage-audio-file))
  (montage-emit-script (format "cat << EOF > %s" (expand-file-name "concat.list" montage-ffmpeg-directory)))
  (cl-dolist (video (mapcar 'montage-ffmpeg-output-file (mapcar 'montage-upscaled-image images)))
    (montage-emit-script (format "file '%s'" video)))
  (montage-emit-script "EOF")
  (montage-emit-script 
   (format "ffmpeg -y -f concat -safe 0 -i %s -i %s -map 0:v -map 1:a -c:v copy %s %s"
           (expand-file-name "concat.list" montage-ffmpeg-directory)
           (or audio (expand-file-name "silence.wav" montage-source-directory))
           (if audio "-shortest" "")
           (expand-file-name "concat.mp4" montage-ffmpeg-directory))))

;;; Upscaling source images into Magick Pixel Cache files

(cl-defun montage-upscaled-image (image)
  (expand-file-name
   (concat (file-name-sans-extension (file-name-nondirectory image))
           "-upscaled.png")
   montage-upscaled-directory))

(cl-defun montage-upscaled-magick-cache (image)
  (expand-file-name
   (concat (file-name-sans-extension (file-name-nondirectory image))
           "-upscaled.mpc")
   montage-upscaled-directory))

(defun montage-upscale-command (file)
  (let ((output-file (montage-upscaled-image file)))
    (format "2>&1 %s -i %s -o %s -m %s -n realesrgan-x4plus"
            montage-upscale-program
            file
            (expand-file-name output-file montage-upscaled-directory)
            montage-upscale-model-directory
            )))

(defun montage-upscale-magick-cache-command (file)
  (let ((input-file (montage-upscaled-image file))
        (output-file (montage-upscaled-magick-cache file)))
    (format "%s convert %s %s" montage-magick-program input-file output-file)))

(defun montage-upscale-emit-script (files)
  (with-temp-buffer
    (cl-dolist (file files)
      (montage-emit-script (montage-upscale-command file))
      (montage-emit-script (montage-upscale-magick-cache-command file)))))

(defun montage-upscale-images-maybe (images &optional force)
  (let (batch)
    (cl-dolist (image images)
      (when (or force (not (file-exists-p (montage-upscaled-magick-cache image))))
        (push image batch)))
    (montage-upscale-emit-script (reverse batch))))

;;; Stable Diffusion scripting

(defcustom montage-diffusion-device "cuda"
  "Pytorch device identifier for Stable Diffusion compute."
  :tag "Montage diffusion device"
  :group 'montage
  :type 'string)

(defun montage-diffusion-script-file (id)
  (expand-file-name (format "montage-%d.py" id) montage-diffusion-directory))

(defun montage-prompt-output-file (id)
  (expand-file-name (format "montage-%d.prompt.txt" id) montage-diffusion-directory))

(cl-defun montage-expand-prompt-template (specs)
  (mapconcat #'identity
             (mapcar (lambda (spec)
                       (if (listp spec) (eval spec) spec))
                     specs)
             ", "))

(cl-defun montage-text-to-image (&key (height 512) (width 512)
                                      prompt output-prefix
                                      (negative-prompt "")
                                      id (steps 50) (guidance 0.75)
                                      (seed (montage-random-seed)))
  (with-temp-buffer
    (let ((template "from os import environ
import random
import torch
from diffusers import StableDiffusionPipeline

prompt = %S
negative_prompt = %S
seed = %d
generator = torch.Generator(\"cuda\").manual_seed(seed)
output_file = \"%s\" + str(%d) + \"-\" + str(seed) + \".png\"
steps = %d
guidance = %f

model_id = \"CompVis/stable-diffusion-v1-4\"

pipe = StableDiffusionPipeline.from_pretrained(
    model_id,
    torch_dtype=torch.float32,
    use_auth_token=True)

pipe = pipe.to(%S)
image = pipe(prompt, negative_prompt=negative_prompt, height=%d, width=%d, guidance_scale=guidance, num_inference_steps=steps, generator=generator)[\"sample\"][0]  
image.save(output_file)
"))
      (insert (format template prompt negative-prompt seed output-prefix id steps guidance (or montage-diffusion-device "cuda") height width))
      (write-file (montage-diffusion-script-file id))
      (with-temp-buffer
        (insert prompt)
        (write-file (montage-prompt-output-file id))))))

(cl-defun montage-generate-images (&key (n 5) (steps 50)
                                        prompt negative-prompt
                                        (guidance 0.75)
                                        montage-diffusion-device
                                        (height 512) (width 512)
                                        seed)
  (let (scripts)
    (dotimes (i n)
      (montage-text-to-image :prompt (montage-expand-prompt-template prompt)
                              :output-prefix (concat montage-container-path-prefix "montage-")
                              :steps steps
                              :id i
                              :seed (or seed (montage-random-seed))
                              :guidance guidance
                              :height height
                              :width width)
      (push (montage-diffusion-script-file i) scripts))
    (reverse scripts)))

(cl-defun montage-image-to-image (&key (height 512) (width 512)
                                       prompt input-file id
                                       (output-prefix montage-container-path-prefix)
                                       (steps 50) (guidance 0.75)
                                       (strength 0.75)
                                       (seed (montage-random-seed)))
  (with-temp-buffer
    (let ((prefix (format "montage-img2img-%d-%d" id seed))
          (template "from os import environ
import torch
from PIL import Image
from diffusers import StableDiffusionImg2ImgPipeline

input_file = %S
output_file = %S 
prompt = %S
seed = %d

init_image = Image.open(input_file).convert(\"RGB\")
init_image = init_image.resize((%d, %d))

generator = torch.Generator(\"cuda\").manual_seed(seed)
model_id_or_path = \"CompVis/stable-diffusion-v1-4\"
pipe = StableDiffusionImg2ImgPipeline.from_pretrained(
    model_id_or_path,
    torch_dtype=torch.float32,
    use_auth_token=True)

pipe = pipe.to(%S)

images = pipe(prompt=prompt, generator=generator, num_inference_steps=%d, init_image=init_image, strength=%f, guidance_scale=%f).images

images[0].save(output_file)"))
      (insert (format template
                      (concat output-prefix (file-name-nondirectory input-file))
                      (concat output-prefix (concat prefix ".png"))
                      prompt seed width height (or montage-diffusion-device "cuda") steps strength guidance))
      (write-file (expand-file-name (concat prefix ".py") montage-diffusion-directory)))))

(cl-defun montage-modify-image (&key image (n 1) (steps 50) prompt (strength 0.75)  (guidance 0.75) (height 512) (width 512) seed)
  (let (scripts)
    (cl-dotimes (i n)
      (montage-image-to-image :height height :width width
                              :prompt (montage-expand-prompt-template prompt)
                              :steps steps
                              :id i
                              :input-file image
                              :seed (or seed (montage-random-seed))
                              :guidance guidance
                              :strength strength))))
                                     

;;; Noise functions

(cl-defun random-choose (items)
  (nth (random (length items)) items))

(cl-defun montage-random-strings (n strings)
  (let (results)
    (cl-dotimes (i n)
      (push (random-choose strings) results))
    (mapconcat 'identity results " ")))

(cl-defun montage-distinct-strings (n strings)
  (cl-assert (> (length strings) n))
  (let (results)
    (while (> n (length results))
      (cl-pushnew (random-choose strings) results :test 'equal))
    (mapconcat 'identity results " ")))

;;; Org mode timeline 

(cl-defun montage-generate-orgtable (images)
  (switch-to-buffer (get-buffer-create "*montage-orgtable*"))
  (delete-region (point-min) (point-max))
  (insert "|duration|transform|image|\n")
  (insert "|-\n")
  (cl-dolist (image images)
    (insert (format "| 8 | ([montage-pan-down :speed 1.00]) | [[file:%s]] | \n" image))))

(cl-defun montage-timeline-images (timeline)
  (mapcar 'third (mapcar 'montage-preprocess-timeline-row timeline)))

(cl-defun montage-preprocess-timeline-row (row)
  (list (first row)
        (montage-pre-expand-op-form
         (list
          (second row)))
        ;; strip any org-mode markup
        (let ((file (third row)))
          (if (= ?\[ (aref file 0))
              (substring file 7 (- (length file) 2))
            file))))

(defun strip-script (s)
  (cl-substitute ?_ ?\n (cl-substitute ?_ ?  s)))

(defun montage-script= (a b)
  (let ((sa (strip-script a))
        (sb (strip-script b)))
    (string= sa sb)))

(cl-defun montage-process-timeline-entry (row)
  (cl-destructuring-bind (duration transform image) row
    (let* ((width (* montage-upscale-factor (montage-image-width image)))
           (height (* montage-upscale-factor (montage-image-height image)))
           (num-frames (montage-span-length duration montage-current-beat-index))
           (script-lines (montage-magick-expand-image-commands
                          (montage-upscaled-magick-cache image)
                          num-frames
                          (mapcar 'montage-pre-expand-op-form transform)))
           (script-string (mapconcat #'identity script-lines "\n"))
           (script-file (montage-ffmpeg-script-file image)))
      (if (and (not montage-inhibit-cache)
               (file-exists-p script-file)
               (let ((old-script (with-temp-buffer
                                   (insert-file-contents script-file)
                                   (buffer-substring-no-properties (point-min) (point-max)))))
                 (montage-script= script-string old-script)))
          (prog1 nil (montage-log "Skipping unmodified script %s." script-file))
        (with-temp-buffer
          ;; first save a copy of the whole script for comparison next time
          (insert script-string)
          (montage-log "Writing modified script %s." script-file)
          (write-region (point-min) (point-max) script-file nil nil)
          ;; set up to write each frame's script component to its own file
          (dotimes (i num-frames)
            (let ((line (pop script-lines)))
                (montage-emit-script (format "echo \"%s\" > %s" line (montage-output-frame-script-file i)))))
          ;; feed in the list of shell scripts
          (montage-emit-script (format "cat << EOF > %s"
                                       (expand-file-name "frames.list" montage-magick-directory)))
          (dotimes (i num-frames)
              (montage-emit-script (montage-output-frame-script-file i)))
          (montage-emit-script "EOF")
          ;; pipe the list to GNU Parallel
          (montage-emit-script (format "cat %s | time parallel --jobs=%d sh -x"
                                       (expand-file-name "frames.list" montage-magick-directory)
                                       montage-maximum-jobs))
          ;; merge frames into a video segment
          (montage-combine-frames (montage-ffmpeg-output-file (montage-upscaled-image image)))
          ;; delete leftover PNG frame files
          (montage-clear-frames)
          ;; move along the beat; this probably isn't quite correct
          (cl-incf montage-current-beat-index (truncate duration))
          ;; return the resulting script filename
          script-file)))))

(cl-defun montage-process-timeline (table)
  (montage-log "Montage: Processing image timeline...")
  (let ((inhibit-message t))
    (montage-clear-script)
    (setf montage-current-beat-index 0)
    (let ((images (mapcar 'third (mapcar 'montage-preprocess-timeline-row table))))
      (montage-upscale-images-maybe images montage-inhibit-cache)
      (let ((scripts (delq nil (mapcar 'montage-process-timeline-entry
                                       (mapcar 'montage-preprocess-timeline-row
                                               table)))))
        (montage-concatenate-videos images montage-audio-file)
        (montage-save-script))))
  (montage-log "Montage: Processing image timeline... Done."))

;;; Larger preview in other window

(defcustom montage-preview-buffer-name "*Montage Preview*"
  "Name of buffer to be used for timeline image preview."
  :tag "Montage preview buffer name"
  :group 'montage
  :type 'string)

(defun montage-show-image (image)
  (interactive)
  (save-excursion
    (let ((buffer (get-buffer-create montage-preview-buffer-name)))
      (switch-to-buffer buffer)
      (delete-region (point-min) (point-max))
      (insert-image (create-image image))
      (display-buffer buffer))))

(defun montage-show-image-dwim ()
  (interactive)
  (save-window-excursion
    (save-excursion
      (goto-char (point-at-bol))
      (let ((line (buffer-substring-no-properties (point) (point-at-eol))))
        (let ((pos (string-match "\\[\\[file:\\(.*\\)\\]\\]" line)))
          (let ((file (when pos (match-string 1 line))))
            (when file (montage-show-image file))))))))

(defun montage-show-image-dwim-maybe ()
  (interactive)
  (when (eq major-mode 'org-mode)
    (when (get-buffer-window montage-preview-buffer-name)
      (montage-show-image-dwim))))

;;; Opening a shell window into the docker container

(defcustom montage-docker-shell-buffer-name "*montage-docker*"
  "Name of buffer to be used for the Docker shell."
  :tag "Montage Docker shell buffer name"
  :group 'montage
  :type 'string)

(defcustom montage-docker-init-command
  "sudo docker start admiring_lederberg; sudo docker attach admiring_lederberg"
  "Shell command to attach to the Docker container."
  :tag "Montage Docker init command"
  :group 'montage
  :type 'string)

(defun montage-docker-shell ()
  (interactive)
  (if (get-buffer montage-docker-shell-buffer-name)
      (switch-to-buffer montage-docker-shell-buffer-name)
    (progn
      (shell montage-docker-shell-buffer-name)
      (goto-char (point-max))
      (insert montage-docker-init-command)
      (comint-send-input))))
            
(defun montage-tell-docker-shell (command)
  (save-window-excursion
    (montage-docker-shell)
    (goto-char (point-max))
    (insert command)
    (comint-send-input)))

;;; Squeak integration

(defvar montage-return-file nil)

(defun montage-tell-squeak (&rest args)
  (with-temp-buffer
    (insert (prin1-to-string args))
    (write-file montage-return-file)))

(defvar montage-error-file nil)

(defun montage-error (string)
  (with-temp-buffer
    (insert string)
    (write-file montage-error-file)))

(defmacro montage-catch-errors (form)
  (let ((var (cl-gensym)))
    `(montage-tell-squeak (condition-case ,var (progn ,form (montage-run-script))
                            (error (montage-error
                                    (prin1-to-string ,var)))))))

(defmacro montage-apply-variables (&rest vars)
  `(setq ,@vars))

(provide 'montage)
;;; montage.el ends here

